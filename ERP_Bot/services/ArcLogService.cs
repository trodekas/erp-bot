﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP_Bot.utils;
using System.Diagnostics;
using HtmlAgilityPack;
using ERP_Bot.entities;
using Discord;

namespace ERP_Bot.services
{
    public class ArcLogService
    {
        private FileSystemWatcher arcLogDirectoryWatcher;
        private FileSystemWatcher parsedLogDirectoryWatcher;

        private ArcStatistics statistics;

        IMessageChannel channelToPostArcLogs;

        private List<ArcLog> sentLogsInSession;

        public void StartSendingNewParsedLogs(IMessageChannel channel)
        {
            statistics = new ArcStatistics();
            channelToPostArcLogs = channel;
            sentLogsInSession = new List<ArcLog>();

            StartMonitoringForNewArcLogs(ConfigUtil.GetArcLogDirectory());
            StartMonitoringForNewParsedLogs(ConfigUtil.GetParsedLogDirectory());
        }

        private void StartMonitoringForNewArcLogs(string path)
        {
            arcLogDirectoryWatcher = new FileSystemWatcher()
            {
                Path = path,
                IncludeSubdirectories = true,
                Filter = "*.evtc.zip"
            };
            arcLogDirectoryWatcher.Created += new FileSystemEventHandler(ParseNewArcLog);
            arcLogDirectoryWatcher.EnableRaisingEvents = true;
        }

        private void StartMonitoringForNewParsedLogs(string path)
        {
            parsedLogDirectoryWatcher = new FileSystemWatcher()
            {
                Path = path,
                IncludeSubdirectories = true,
                Filter = "*.html"
            };
            parsedLogDirectoryWatcher.Created += new FileSystemEventHandler(HandleParsedLogCreation);
            parsedLogDirectoryWatcher.EnableRaisingEvents = true;
        }

        private void StopMonitoringForNewArcLogs()
        {
            if (arcLogDirectoryWatcher != null)
            {
                arcLogDirectoryWatcher.Created -= ParseNewArcLog;
                arcLogDirectoryWatcher = null;
            }
        }

        private void StopMonitoringForNewParsedLogs()
        {

            parsedLogDirectoryWatcher.Created -= HandleParsedLogCreation;
            parsedLogDirectoryWatcher = null;
        }

        private void ParseNewArcLog(object source, FileSystemEventArgs e)
        {
            FileUtils.WaitForFile(new FileInfo(e.FullPath));

            ProcessStartInfo raidHeroesStartInfo = new ProcessStartInfo()
            {
                FileName = ConfigUtil.GetRaidHeroesFilePath(),
                Arguments = "\"" + e.FullPath + "\"",
                CreateNoWindow = true,
                UseShellExecute = false
            };
            Process raidHeroesProcess = new Process()
            {
                StartInfo = raidHeroesStartInfo
            };
            raidHeroesProcess.Start();
        }

        private void HandleParsedLogCreation(object source, FileSystemEventArgs e)
        {
            Program.Logger(new LogMessage(LogSeverity.Info, ToString(), "Handling Parsed Log Creation"));

            FileUtils.WaitForFile(new FileInfo(e.FullPath));

            ArcLog arcLog = new ArcLog(e.FullPath);
            Program.Logger(new LogMessage(LogSeverity.Info, ToString(), "Log Parsed for " + arcLog.BossName));

            AddEntriesToStatistics(arcLog);
            SendParsedArcLogToChannel(arcLog, channelToPostArcLogs).GetAwaiter().GetResult();
        }

        //TODO maybe change to property
        public bool IsPostingLogs()
        {
            return arcLogDirectoryWatcher != null && parsedLogDirectoryWatcher != null;
        }

        public async Task StopSendingNewParsedLogs()
        {
            StopMonitoringForNewArcLogs();
            StopMonitoringForNewParsedLogs();

            await SendLastTriesOfEachBossToChannel();
        }

        //TODO change this shit
        private void AddEntriesToStatistics(ArcLog arcLog)
        {
            Program.Logger(new LogMessage(LogSeverity.Info, ToString(), "Adding entries to arc statistics"));

            HtmlDocument parsedLog = new HtmlDocument();
            parsedLog.Load(arcLog.FilePath);

            var statsTable = parsedLog.DocumentNode.SelectSingleNode("//table[@class='table table-condensed table_stats']");

            HtmlNode thead = statsTable.SelectSingleNode("thead");
            HtmlNode trHead = thead.SelectSingleNode("tr");

            List<int> collumnNumbers = new List<int>();
            int i = 0;
            foreach (HtmlNode cell in trHead.SelectNodes("th"))
            {
                if (cell.InnerHtml.Contains("Display Name") || cell.InnerHtml.Contains("Boss DPS") || cell.InnerHtml.Contains("All DPS") || cell.InnerHtml.Contains("Downed"))
                {
                    collumnNumbers.Add(i);
                }

                i++;
            }

            HtmlNode tbody = statsTable.SelectSingleNode("tbody");
            foreach (HtmlNode row in tbody.SelectNodes("tr"))
            {
                HtmlNodeCollection cells = row.SelectNodes("td");

                statistics.Insert(cells.ElementAt(collumnNumbers.ElementAt(0)).InnerText, new ArcStatisticsEntry(Int32.Parse(cells.ElementAt(collumnNumbers.ElementAt(1)).InnerText),
                    Int32.Parse(cells.ElementAt(collumnNumbers.ElementAt(2)).InnerText),
                    Int32.Parse(cells.ElementAt(collumnNumbers.ElementAt(3)).InnerText)));
            }

            parsedLog = null;
        }

        //TODO maybe change to property
        public string GetDownedStats()
        {
            return statistics != null ? statistics.GetSortedDownsListInString() : "Stats are empty";
        }

        public string GetEncounterTriesStats()
        {
            return statistics != null ? statistics.GetTotalEncountersString() : "Stats are empty";
        }

        public string GetAverageBossDpsStats()
        {
            return statistics != null ? statistics.GetSortedAverageBossDpsListInString() : "Stats are empty";
        }

        private async Task SendParsedArcLogToChannel(ArcLog arcLog, IMessageChannel channel)
        {
            sentLogsInSession.Add(arcLog);
            await SendFileToChannel(arcLog.FilePath, channelToPostArcLogs);
        }

        private async Task SendLastTriesOfEachBossToChannel()
        {
            await channelToPostArcLogs.SendMessageAsync("Last tries for each boss in one place");
            List<ArcLog> lastTriesLogs = GetArcLogsForLastTries(sentLogsInSession);
            List<string> filePathList = lastTriesLogs.Select(arcLog => arcLog.FilePath).ToList();
            await SendFileToChannel(CompressMultipleFiles(filePathList), channelToPostArcLogs);
        }

        private async Task SendFileToChannel(String filePath, IMessageChannel channel)
        {      
            if (new FileInfo(filePath).Length < ConfigUtil.GetMaxUploadSize())
            {
                await channel.SendFileAsync(filePath);
            }
            else
            {
                await channel.SendMessageAsync("File is too large to upload, compressing it....");

                string compressedFilePath = CompressFile(filePath);

                if (new FileInfo(compressedFilePath).Length < ConfigUtil.GetMaxUploadSize())
                {
                    await channel.SendFileAsync(compressedFilePath);
                }
                else
                {
                    await channel.SendMessageAsync("Even the compressed file is too large to upload FeelsBadMan");
                }
            }
        }

        private string CompressFile(string filePath)
        {
            string compressedFilePath = filePath + ".zip";

            using (FileStream compressedFileStream = new FileStream(compressedFilePath, FileMode.Create))
            using (ZipArchive compressedFile = new ZipArchive(compressedFileStream, ZipArchiveMode.Create))
            {
                compressedFile.CreateEntryFromFile(filePath, filePath.Split('\\').Last());
            }

            return compressedFilePath;
        }

        private string CompressMultipleFiles(List<string> filePathList)
        {
            string fileName = "resources\\bundled_parsed_logs\\" + DateTime.Now.ToString("yyyy/MM/dd") + ".zip";

            // Create and open a new ZIP file
            ZipArchive zip = ZipFile.Open(fileName, ZipArchiveMode.Create);
            foreach (string filePath in filePathList)
            {
                // Add the entry for each file
                zip.CreateEntryFromFile(filePath, Path.GetFileName(filePath), CompressionLevel.Optimal);
            }
            // Dispose of the object when we are done
            zip.Dispose();

            return fileName;
        }

        private List<ArcLog> GetArcLogsForLastTries(List<ArcLog> arcLogs)
        {
            return arcLogs
                .GroupBy(arcLog => arcLog.BossName)
                .Select(group => group.Last()).ToList();
        }
    }
}
