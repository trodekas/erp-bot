﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ERP_Bot.services
{
    public class GreetingsService
    {
        #region Global variables
        private Dictionary<string, List<string>> personalisedGreetingsDictionary;
        private Dictionary<int, string> randomGreetingsDictionary;
        private bool hasBeenLoaded = false;

        #endregion

        #region Private methods
        private void LoadGreetingsDocument()
        {
            XmlDocument greetingsDoc = new XmlDocument();
            greetingsDoc.Load("resources/Greetings.xml");

            LoadPersonalisedGreetings(greetingsDoc);
            LoadRandomGreetings(greetingsDoc);

            hasBeenLoaded = true;
        }

        private void LoadPersonalisedGreetings(XmlDocument greetingsDoc)
        {
            XmlNodeList resources = greetingsDoc.SelectNodes("greetings/personalisedGreetings/greet");
            personalisedGreetingsDictionary = new Dictionary<string, List<string>>();
            foreach (XmlNode node in resources)
            {
                if (personalisedGreetingsDictionary.TryGetValue(node.Attributes["name"].Value, out List<string> greetingsList))
                {
                    greetingsList.Add(node.InnerText);
                }
                else
                {
                    greetingsList = new List<string>
                    {
                        node.InnerText
                    };
                    personalisedGreetingsDictionary.Add(node.Attributes["name"].Value, greetingsList);
                }
            }
        }

        private void LoadRandomGreetings(XmlDocument greetingsDoc)
        {
            XmlNodeList resources = greetingsDoc.SelectNodes("greetings/randomGreetings/greet");
            randomGreetingsDictionary = new Dictionary<int, string>();
            foreach (XmlNode node in resources)
            {
                randomGreetingsDictionary.Add(Int32.Parse(node.Attributes["id"].Value), node.InnerText);
            }
        }

        #endregion

        #region Public methods
        public string GetGreeting(string name)
        {
            if (!hasBeenLoaded)
            {
                LoadGreetingsDocument();
            }

            Random random = new Random();

            if (personalisedGreetingsDictionary.TryGetValue(name, out List<string> greetingList))
            {
                return greetingList[random.Next(greetingList.Count)];
            }
            else
            {
                random.Next(randomGreetingsDictionary.Count);
                if (randomGreetingsDictionary.TryGetValue(random.Next(randomGreetingsDictionary.Count), out string greeting))
                {
                    return greeting;
                }
            }

            return "Hello";
        }

        #endregion
    }
}
