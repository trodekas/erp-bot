﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ERP_Bot.services
{
    public class FarewellsService
    {
        #region Global variables
        private Dictionary<string, List<string>> personalisedFarewellsDictionary;
        private List<string> randomFarewellsList;
        private bool hasBeenLoaded = false;

        #endregion

        #region Public methods
        private void LoadFarewellsDocument()
        {
            XmlDocument farewellsDoc = new XmlDocument();
            farewellsDoc.Load("resources/Farewells.xml");

            LoadPersonalisedGreetings(farewellsDoc);
            LoadRandomFarewells(farewellsDoc);

            hasBeenLoaded = true;
        }

        private void LoadPersonalisedGreetings(XmlDocument farewellsDoc)
        {
            XmlNodeList resources = farewellsDoc.SelectNodes("farewells/personalisedFarewells/farewell");
            personalisedFarewellsDictionary = new Dictionary<string, List<string>>();
            foreach (XmlNode node in resources)
            {
                if (personalisedFarewellsDictionary.TryGetValue(node.Attributes["name"].Value, out List<string> farewellsList))
                {
                    farewellsList.Add(node.InnerText);
                }
                else
                {
                    farewellsList = new List<string>
                    {
                        node.InnerText
                    };
                    personalisedFarewellsDictionary.Add(node.Attributes["name"].Value, farewellsList);
                }
            }
        }

        private void LoadRandomFarewells(XmlDocument greetingsDoc)
        {
            XmlNodeList resources = greetingsDoc.SelectNodes("farewells/randomFarewells/farewell");
            randomFarewellsList = new List<string>();
            foreach (XmlNode node in resources)
            {
                randomFarewellsList.Add(node.InnerText);
            }
        }

        #endregion

        #region Public methods
        public string GetFarewell(string name)
        {
            if (!hasBeenLoaded)
            {
                LoadFarewellsDocument();
            }

            Random random = new Random();

            if (personalisedFarewellsDictionary.TryGetValue(name, out List<string> farewellsList))
            {
                return farewellsList[random.Next(farewellsList.Count)];
            }
            else
            {
                return randomFarewellsList.ElementAt(random.Next(randomFarewellsList.Count));
            }
        }

        #endregion
    }
}
