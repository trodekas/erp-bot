﻿using ERP_Bot.entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YoutubeExplode;
using YoutubeExplode.Models;

namespace ERP_Bot.services
{
    public class VideoService
    {
        public async Task<YoutubeFileInfo> DownloadYoutubeVideo(string requester, string url)
        {
            // Client
            var client = new YoutubeClient();

            // Get the video ID
            string id = url;
            id = NormalizeId(id);

            // Get the video info
            var videoInfo = await client.GetVideoInfoAsync(id);

            await Program.Logger(new Discord.LogMessage(Discord.LogSeverity.Info, ToString(), videoInfo.Title));

            // Get the most preferable stream
            var streamInfo = videoInfo.MixedStreams
                .OrderBy(s => s.VideoQuality)
                .Last();

            // Compose file name, based on metadata
            string fileExtension = streamInfo.Container.GetFileExtension();
            string fileName = videoInfo.Title + "." + fileExtension;

            // Remove illegal characters from file name
            string invalidChars = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            foreach (char c in invalidChars)
            {
                fileName = fileName.Replace(c.ToString(), "");
            }

            fileName = "resources/music/" + fileName;

            if(File.Exists(fileName))
            {
                await Program.Logger(new Discord.LogMessage(Discord.LogSeverity.Info, ToString(), "File already exists, no need for downloading"));
                return new YoutubeFileInfo(url, fileName, requester); 
            }

            await client.DownloadMediaStreamAsync(streamInfo, fileName);

            await Program.Logger(new Discord.LogMessage(Discord.LogSeverity.Info, ToString(), "Downloaded"));

            return new YoutubeFileInfo(url, fileName, requester);
        }

        private static string NormalizeId(string input)
        {
            if (!YoutubeClient.TryParseVideoId(input, out string id))
            {
                id = input;
            }
                
            return id;
        }
    }
}
