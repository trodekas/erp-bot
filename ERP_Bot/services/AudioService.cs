﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Discord;
using Discord.Audio;
using Discord.Commands;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using ERP_Bot.entities;

namespace ERP_Bot.services
{
    public class AudioService
    {
        private IAudioClient audioClient;
        private bool SendingAudio { get; set; } = false;
        public bool Loop { get; set; } = false;

        private ConcurrentQueue<YoutubeFileInfo> songQueue = new ConcurrentQueue<YoutubeFileInfo>();
        private YoutubeFileInfo currentAudioFileInfo;

        private Stream currentAudioStream;
        private AudioOutStream currentAudioOutStream;

        public async Task JoinAudioChannel(IVoiceChannel target)
        {
            audioClient = await target.ConnectAsync();
        }

        public async Task LeaveAudio()
        {
            if(audioClient != null)
            {
                await audioClient.StopAsync();
            }     
        }

        public void AddToSongQueue(YoutubeFileInfo audioFileInfo)
        {
            songQueue.Enqueue(audioFileInfo);
        }

        public async Task PlaySongQueue(IMessageChannel channel)
        {
            SendingAudio = true;
            while (songQueue.Count > 0)
            {
                songQueue.TryDequeue(out currentAudioFileInfo);
                await SendAudioAsync(channel, currentAudioFileInfo);
                while(Loop)
                {
                    await SendAudioAsync(channel, currentAudioFileInfo);
                }
            }
            SendingAudio = false;
        }

        private async Task SendAudioAsync(IMessageChannel channel, YoutubeFileInfo audioFileInfo)
        {
            if (!File.Exists(audioFileInfo.FileName))
            {
                await channel.SendMessageAsync("File does not exist.");
                return;
            }
 
            currentAudioStream = CreateStream(audioFileInfo.FileName).StandardOutput.BaseStream;
            currentAudioOutStream = audioClient.CreatePCMStream(AudioApplication.Music);

            try
            {
                await currentAudioStream.CopyToAsync(currentAudioOutStream);
                await currentAudioOutStream.FlushAsync().ConfigureAwait(false);
            }
            catch
            {
                return;
            }
        }

        public async Task StopAudioOutput()
        {
            if(currentAudioStream != null && currentAudioOutStream != null)
            {
                currentAudioStream.Close();
                await currentAudioOutStream.FlushAsync().ConfigureAwait(false);
            }               
        }

        private Process CreateStream(string path)
        {
            return Process.Start(new ProcessStartInfo
            {
                FileName = "ffmpeg.exe",
                Arguments = $"-hide_banner -loglevel panic -i \"{path}\" -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true
            });
        }

        public bool IsAudioClientConnected()
        {
            if(audioClient == null)
            {
                return false;
            }
            else if(audioClient.ConnectionState == ConnectionState.Connected || audioClient.ConnectionState == ConnectionState.Connecting)
            {
                return true;
            }

            return false;
        }

        public bool IsSongQueuePlaying()
        {
            return SendingAudio;
        }

        public int GetQueueLength()
        {
            return songQueue.Count;
        }

        public YoutubeFileInfo GetLastPlayedAudioInfo()
        {
            return currentAudioFileInfo;
        }
    }
}
