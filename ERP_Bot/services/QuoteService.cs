﻿using ERP_Bot.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ERP_Bot.services
{
    public class QuoteService
    {
        #region Global variables
        private List<Quote> quoteList;
        private bool hasBeenLoaded = false;

        #endregion

        #region Private methods
        private void LoadQuotesDocument()
        {
            XmlDocument quotesDoc = new XmlDocument();
            quotesDoc.Load("resources/Quotes.xml");

            LoadQuotes(quotesDoc);

            hasBeenLoaded = true;
        }

        private void LoadQuotes(XmlDocument quotesDoc)
        {
            XmlNodeList resources = quotesDoc.SelectNodes("quotes/quote");
            quoteList = new List<Quote>();

            foreach (XmlNode node in resources)
            {
                quoteList.Add(new Quote(node.Attributes["name"].Value, node.InnerText));
            }
        }

        #endregion

        #region Public methods
        public Quote GetQuote()
        {
            if(!hasBeenLoaded)
            {
                LoadQuotesDocument();
            }
            Random random = new Random();

            return quoteList.ElementAt(random.Next(quoteList.Count));
        }

        #endregion
    }
}
