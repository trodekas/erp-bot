﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using ERP_Bot.utils;
using ERP_Bot.services;

namespace ERP_Bot
{
    public class BasicCommandsModule : ModuleBase<ICommandContext>
    {
        #region Global variables
        private readonly GreetingsService greetingsService;
        private readonly FarewellsService farewellsService;
        private readonly QuoteService quoteService;

        #endregion

        #region Commands
        public BasicCommandsModule(GreetingsService greetingsService, FarewellsService farewellsService, QuoteService quoteService)
        {
            this.greetingsService = greetingsService;
            this.farewellsService = farewellsService;
            this.quoteService = quoteService;
        }

        [Command("hi"), Alias("hello", "welcome", "sup", "hola", "joho"), Summary("Greets the user")]
        public async Task HiCommand()
        {
            await Context.Channel.SendMessageAsync(greetingsService.GetGreeting(Context.User.Username));
        }

        [Command("hi"), Alias("hello", "welcome", "sup", "hola", "joho"), Summary("Greets the user")]
        public async Task HiCommand([Remainder] string argument)
        {
            if (argument.ToLower().Equals("bitch"))
            {
                await Context.Channel.SendMessageAsync("Don't you dare call me like that, you Biatch");
            }
        }

        [Command("bye"), Alias("night", "cya")]
        public async Task ByeCommand()
        {
            await Context.Channel.SendMessageAsync(farewellsService.GetFarewell(Context.User.Username));
        }

        [Command("bye"), Alias("night", "cya")]
        public async Task ByeCommand([Remainder] string argument)
        {
            if(argument.ToLower().Equals("bitch"))
            {
                await Context.Channel.SendMessageAsync("Don't you dare call me like that, you Biatch");
            }
        }

        [Command("quote")]
        public async Task QuoteCommand()
        {
            await Context.Channel.SendMessageAsync(quoteService.GetQuote().ToString());
        }
        #endregion
    }
}
