﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using ERP_Bot.services;

namespace ERP_Bot.modules
{
    public class ArcLogCommandsModule : ModuleBase<ICommandContext>
    { 
        private readonly ArcLogService arcLogService;

        public ArcLogCommandsModule(ArcLogService arcLogService)
        {
            this.arcLogService = arcLogService;
        }

        [Command("start")]
        [RequireOwner]
        public async Task StartPostingParsedLogsCommand()
        {
            if(!arcLogService.IsPostingLogs())
            {
                arcLogService.StartSendingNewParsedLogs(Context.Channel);
                await Context.Channel.SendMessageAsync("I will post arc logs now");
            }
            else
            {
                await Context.Channel.SendMessageAsync("I am already doing that");
            }
        }

        [Command("stop")]
        [RequireOwner]
        public async Task StopPostingParsedLogsCommand()
        {
           if(arcLogService.IsPostingLogs())
           {
                await arcLogService.StopSendingNewParsedLogs();
                await Context.Channel.SendMessageAsync("I will no longer post arc logs");
           }
           else
           {
                await Context.Channel.SendMessageAsync("I cannot stop what is not happening");
           }
        }

        [Command("statistics")]
        [Alias("stats")]
        public async Task StatisticsCommand()
        {
            await Context.Channel.SendMessageAsync("```" + arcLogService.GetDownedStats() + "```");
            await Context.Channel.SendMessageAsync("```" + arcLogService.GetAverageBossDpsStats() + "```");
            await Context.Channel.SendMessageAsync("```" + arcLogService.GetEncounterTriesStats() + "```");
        }
    }
}
