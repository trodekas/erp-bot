﻿using Discord;
using Discord.Commands;
using ERP_Bot.utils;
using System.Linq;
using System.Threading.Tasks;

namespace ERP_Bot.modules
{
    public class HelpCommandsModule : ModuleBase<ICommandContext>
    {
        private readonly CommandService commandService;

        public HelpCommandsModule(CommandService commandService)
        {
            this.commandService = commandService;
        }

        [Command("help")]
        [Alias("commands", "command", "info")]
        public async Task HelpAsync()
        {
            string prefix = ConfigUtil.GetCommandPrefix().ToString();
            var builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = "These are the commands you can use"
            };

            foreach (var module in commandService.Modules)
            {
                string description = null;
                foreach (CommandInfo command in module.Commands)
                {
                    var result = await command.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess)
                        description += $"{prefix}{command.Aliases.First()}\n";
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.Name;
                        x.Value = description;
                        x.IsInline = false;
                    });
                }
            }

            await ReplyAsync("", false, builder.Build());
        }

        [Command("help")]
        [Alias("commands","command","info")]
        public async Task HelpAsync(string command)
        {
            var result = commandService.Search(Context, command);

            if (!result.IsSuccess)
            {
                await ReplyAsync($"Sorry, I couldn't find a command like **{command}**.");
                return;
            }

            EmbedBuilder embedBuilder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = $"Here are some commands like **{command}**"
            };

            foreach (var match in result.Commands)
            {
                CommandInfo cmd = match.Command;

                embedBuilder.AddField(x =>
                {
                    x.Name = string.Join(", ", cmd.Aliases);
                    x.Value = $"Parameters: {string.Join(", ", cmd.Parameters.Select(p => p.Name))}\n" +
                              $"Remarks: {cmd.Remarks}";
                    x.IsInline = false;
                });
            }

            await ReplyAsync("", false, embedBuilder.Build());
        }
    }
}
