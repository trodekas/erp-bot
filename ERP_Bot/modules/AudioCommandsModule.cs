﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using ERP_Bot.entities;
using ERP_Bot.services;

namespace ERP_Bot
{
    public class AudioCommandsModule : ModuleBase<ICommandContext>
    {
        private readonly AudioService audioService;
        private readonly VideoService videoService;

        public AudioCommandsModule(AudioService audioService, VideoService videoService)
        {
            this.audioService = audioService;
            this.videoService = videoService;
        }

        [Command("join", RunMode = RunMode.Async)]
        public async Task JoinCommand()
        {
            await audioService.JoinAudioChannel((Context.User as IVoiceState).VoiceChannel);
        }

        [Command("join", RunMode = RunMode.Async)]
        public async Task JoinCommand([Remainder] string channelId)
        {
            IChannel channel = await Context.Client.GetChannelAsync(ulong.Parse(channelId));
            await audioService.JoinAudioChannel(channel as IVoiceChannel);
        }

        [Command("leave", RunMode = RunMode.Async)]
        public async Task LeaveCommand()
        {
            if(audioService.IsAudioClientConnected())
            {
                await audioService.LeaveAudio();
            }
            else
            {
                await Context.Channel.SendMessageAsync("I am not in voice channel");
            }
        }

        [Command("play", RunMode = RunMode.Async)]
        public async Task PlayCommand([Remainder] string songUrl)
        {
            if((Context.User as IVoiceState).VoiceChannel != null || audioService.IsAudioClientConnected())
            {
                if(audioService.IsAudioClientConnected())
                {
                    if(!audioService.IsSongQueuePlaying())
                    {
                        await AddSongToQueue(songUrl);
                        await audioService.PlaySongQueue(Context.Channel);
                    }
                    else
                    {
                        await AddSongToQueue(songUrl);
                        await Context.Channel.SendMessageAsync("Your song is " + audioService.GetQueueLength() + " in queue");
                    }
                }
                else
                {
                    await audioService.JoinAudioChannel((Context.User as IVoiceState).VoiceChannel);
                    await AddSongToQueue(songUrl);
                    await audioService.PlaySongQueue(Context.Channel);
                }
            }
            else
            {
                await Context.Channel.SendMessageAsync("You must be in voice channel to use this command");
            }
        }

        private async Task AddSongToQueue(string songUrl)
        {
            YoutubeFileInfo songInfo = await videoService.DownloadYoutubeVideo(Context.User.Username, songUrl);
            audioService.AddToSongQueue(songInfo);
        }

        [Command("play")]
        public async Task PlayCommand()
        {
            await Context.Channel.SendMessageAsync("Please enter youtube link next time, you ape");
        }

        [Command("skip")]
        public async Task SkipCommand()
        {
            if(audioService.IsSongQueuePlaying())
            {
                audioService.Loop = false;
                await audioService.StopAudioOutput();
                await Context.Channel.SendMessageAsync("Skipped it");
            }
            else
            {
                await Context.Channel.SendMessageAsync("Nothing is playing");
            }
        }

        [Command("loop")]
        public async Task LoopCommand()
        {
            audioService.Loop = !audioService.Loop;

            await Context.Channel.SendMessageAsync(audioService.Loop == true ? "Looping current song" : "Stopped the loop");
        }

        [Command("current")]
        public async Task CurrentAudioCommand()
        {
            if(audioService.GetLastPlayedAudioInfo() != null)
            {
                await Context.Channel.SendMessageAsync("Current song (if I can call it a song): " + audioService.GetLastPlayedAudioInfo().Url + " requested by " + audioService.GetLastPlayedAudioInfo().Requester);
            }
            else
            {
                await Context.Channel.SendMessageAsync("Nothing played you dumbass");
            }
        }
    }
}
