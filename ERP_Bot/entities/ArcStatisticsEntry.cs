﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_Bot.entities
{
    public class ArcStatisticsEntry
    {
        public int BossDps { get; }
        public int AllDps { get; }
        public int DownedTimes { get; }

        public ArcStatisticsEntry(int bossDps, int allDps, int downedTimes)
        {
            this.BossDps = bossDps;
            this.AllDps = allDps;
            this.DownedTimes = downedTimes;
        }
    }
}
