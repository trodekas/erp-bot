﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_Bot.entities
{
    public class ArcLog
    {
        public string BossName { get; }
        public string FilePath { get; }

        public ArcLog(string filePath)
        {
            string[] splitedPath = filePath.Split('.');
            string bossName = splitedPath.ElementAt(splitedPath.Length - 2);

            BossName = bossName;
            FilePath = filePath;
        }

        public override string ToString()
        {
            return BossName + " " + FilePath;
        }
    }
}
