﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_Bot.entities
{
    public class YoutubeFileInfo
    {
        public string Url { get; }
        public string FileName { get; }
        public string Requester { get; }

        public YoutubeFileInfo(string url, string fileName, string requester)
        {
            Url = url;
            FileName = fileName;
            Requester = requester;
        }
    }
}
