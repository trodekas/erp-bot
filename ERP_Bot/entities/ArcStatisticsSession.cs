﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_Bot.entities
{
    public class ArcStatisticsSession
    {
        private List<ArcStatisticsEntry> statsList = new List<ArcStatisticsEntry>();

        public string DisplayName { get; }

        public ArcStatisticsSession(string displayName, ArcStatisticsEntry entry)
        {
            DisplayName = displayName;
            Add(entry);
        }

        public int TotalDownedTimes
        {
            get
            {
                int totalDownedTimes = 0;
                foreach (ArcStatisticsEntry entry in statsList)
                {
                    totalDownedTimes += entry.DownedTimes;
                }

                return totalDownedTimes;
            }
        }

        public int EncountersCount
        {
            get
            {
                return statsList.Count;
            }
        }

        public int AverageBossDps
        {
            get
            {
                return (int)statsList.Average(arcStats => arcStats.BossDps);
            }
           
        }

        public void Add(ArcStatisticsEntry entry)
        {
            statsList.Add(entry);
        }
    }
}
