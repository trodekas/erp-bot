﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_Bot.entities
{
    public class ArcStatistics
    {
        private Dictionary<string, ArcStatisticsSession> statistics = new Dictionary<string, ArcStatisticsSession>();

        public void Insert(string displayName, ArcStatisticsEntry entry)
        {
            if (statistics.TryGetValue(displayName, out ArcStatisticsSession statsSession))
            {
                statsSession.Add(entry);
            }
            else
            {
                statsSession = new ArcStatisticsSession(displayName, entry);
                statistics.Add(displayName, statsSession);
            }

        }

        /// <summary>
        /// Creates a sorted list of total downed states in a current statistics session
        /// </summary>
        /// <returns>A list in string format</returns>
        public string GetSortedDownsListInString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            List<ArcStatisticsSession> sessions = statistics.Values.ToList();
            sessions.Sort((x, y) => y.TotalDownedTimes.CompareTo(x.TotalDownedTimes));

            stringBuilder.Append("Total downs:");
            stringBuilder.AppendLine();

            int i = 1;
            foreach(ArcStatisticsSession session in sessions)
            {
                stringBuilder.AppendFormat("{0,2}) {1,-20} {2,2}", i++, session.DisplayName, session.TotalDownedTimes);
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        public string GetSortedAverageBossDpsListInString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            List<ArcStatisticsSession> sessions = statistics.Values.ToList();
            sessions.Sort((x, y) => y.AverageBossDps.CompareTo(x.AverageBossDps));

            stringBuilder.Append("Average boss dps:");
            stringBuilder.AppendLine();

            int i = 1;
            foreach (ArcStatisticsSession session in sessions)
            {
                stringBuilder.AppendFormat("{0,2}) {1,-20} {2,2}", i++, session.DisplayName, session.AverageBossDps);
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        public string GetTotalEncountersString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Total encounter tries:");
            stringBuilder.AppendLine();

            List<ArcStatisticsSession> sessions = statistics.Values.ToList();
            sessions.Sort((x, y) => y.EncountersCount.CompareTo(x.EncountersCount));
            return stringBuilder.Append(sessions.First().EncountersCount.ToString()).ToString();
        }
    }
}
