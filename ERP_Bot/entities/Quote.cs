﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP_Bot.entities
{
    public class Quote
    {
        public string Name { get; }
        public string Text { get; }
        
        public Quote(string name, string text)
        {
            Name = name;
            Text = text;
        }

        public override string ToString()
        {
            return Text + " - " + Name;
        }
    }
}
