﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ERP_Bot.utils
{
    class ConfigUtil
    {
        //TODO change to getters
        private static readonly string CommandPrefix = "CommandPrefix";
        private static readonly string ServerToken = "ServerToken";
        private static readonly string ArcLogDirectory = "ArcLogDirectory";
        private static readonly string ParsedLogDirectory = "ParsedLogDirectory";
        private static readonly string RaidHeroesFilePath = "RaidHeroesFilePath";
        private static readonly string MaxUploadSize = "MaxUploadSize";
        private static readonly string MasterName = "MasterName";

        public static bool CheckIfConfigurated()
        {
            if (GetCommandPrefix().ToString().Length == 0) return false;
            if (GetServerToken().Length == 0) return false;
            if (GetArcLogDirectory().Length == 0) return false;
            if (GetRaidHeroesFilePath().Length == 0) return false;
            if (GetMaxUploadSize().ToString().Length == 0) return false;

            return true;
        }

        public static char GetCommandPrefix()
        {
            return ConfigurationManager.AppSettings.Get(CommandPrefix).ElementAt(0);
        }

        public static string GetServerToken()
        {
            return ConfigurationManager.AppSettings.Get(ServerToken);
        }

        public static string GetArcLogDirectory()
        {
            return ConfigurationManager.AppSettings.Get(ArcLogDirectory);
        }

        public static string GetRaidHeroesFilePath()
        {
            return ConfigurationManager.AppSettings.Get(RaidHeroesFilePath);
        }

        public static int GetMaxUploadSize()
        {
            return Int32.Parse(ConfigurationManager.AppSettings.Get(MaxUploadSize));
        }

        public static string GetMasterName()
        {
            return ConfigurationManager.AppSettings.Get(MasterName);
        }

        public static string GetParsedLogDirectory()
        {
            return ConfigurationManager.AppSettings.Get(ParsedLogDirectory);
        }
    }
}
