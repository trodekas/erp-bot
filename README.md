### What is this repository for? ###

Bot for discord written in C# using [Discord .NET API][0]

Use cases:

- Scan, parse and upload new .evtc logs created by [ArcDPS][1]

- Create statistics from parsed logs

- Play music on voice channel

- Others misc commands

[0]: https://github.com/RogueException/Discord.Net/tree/dev
[1]: https://www.deltaconnected.com/arcdps/